import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 加载动态设置rem基准值
import 'amfe-flexible'

// 加载reset-css全局样式初始化
import 'reset-css'

// 加载Vant组件库
import Vant from 'vant'
import 'vant/lib/index.css'

// 加载全局样式
import './styles/index.less'

Vue.config.productionTip = false

// 注册使用Vant
Vue.use(Vant)

// 价格过滤器
Vue.filter('RMBformat', val => {
  return '￥' + Number(val).toFixed(2) + '元'
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
