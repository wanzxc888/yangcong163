import request from './request'

// 请求首页数据
export const GetHomeData = () => request.get('/index/index')

// 请求搜索历史和热门搜索数据
export const GetSearchHistoryHot = () => request.get('/search/index')

// 完整的写法
// export const GetSearchSuggestion = (params) => request.get('/search/helper', { params: params })
// 请求实时搜索联想数据
export const GetSearchSuggestion = params => request.get('/search/helper', { params })

// 请求实时搜索结果列表数据
export const GetGoodsListData = params => request.get('/goods/list', { params })

// 请求删除搜索历史记录数据
export const PostDeleteData = () => request.post('/search/clearhistory')

// 请求品牌制造商详情数据
export const GetBrandDetailData = params => request.get('/brand/detail', { params })

// 请求专题列表数据
export const GetSpecialListData = params => request.get('/topic/list', { params })

// 请求分类页面数据
export const PostCategoryIndexData = () => request.post('/catalog/index')

// 请求分类点击切换数据
export const GetCategoryCurrentData = params => request.get('/catalog/current', { params })

// 请求登录数据
export const PostLoginData = params => request.post('/auth/loginByWeb', params)

// 请求产品列表页数据
export const GetChannelGoodsListData = params => request.get('/goods/category', { params })

// 请求产品详情页数据
export const GetGoodsDetailData = params => request.get('/goods/detail', { params })

// 请求产品详情页-相关产品列表-数据
export const GetGoodsRelatedData = params => request.get('/goods/related', { params })

// 请求产品详情页-工具栏-购物车图标显示的产品数量-数据
export const GetGoodsCountData = () => request.get('/cart/goodscount')

// 请求产品详情页-工具栏-加入购物车的产品数量-数据（添加购物车）
export const PostAddCartData = params => request.post('/cart/add', params)

// 请求购物车页面列表数据
export const GetCartListData = () => request.get('/cart/index')

// 请求购物车页面点击切换商品选中状态（含全选）
export const PostCartCheckedData = params => request.post('/cart/checked', params)

// 请求购物车页面步进器，加减按钮
export const PostStepperData = params => request.post('/cart/update', params)

// 请求购物车页面点击删除
export const PostDeleteGoodsData = params => request.post('/cart/delete', params)
